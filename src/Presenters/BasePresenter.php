<?php

declare(strict_types=1);

namespace JaAdmin\TextPageModule\Presenters;

use JaAdmin\CoreModule\Presenters\SecurePresenter;
use JaAdmin\TextPageModule\Services\TextPageService;
use Nette\DI\Attributes\Inject;

class BasePresenter extends SecurePresenter
{
    protected const ExtensionName = "textPage";
    private const RedirectLink = ":Core:Overview:default";

    #[Inject]
    public TextPageService $textPageService;

    public function startup()
    {
        parent::startup();

        if (!$this->extensionService->getItem(self::ExtensionName)->isActive()) {
            $this->redirect(self::RedirectLink);
        }
    }
}
