<?php

declare(strict_types=1);

namespace JaAdmin\TextPageModule\Presenters;

use Exception;
use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use JaAdmin\CoreModule\Utils\Privilege;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Tracy\ILogger;

final class AddPresenter extends BasePresenter
{
    private const RedirectLink = ":TextPage:Overview:default";
    private const PermissionFail = "textPageModule.add.flashMessage.permissionFail";
    private const AddSuccess = "textPageModule.add.flashMessage.addSuccess";
    private const AddFail = "textPageModule.add.flashMessage.addFail";

    public function actionDefault()
    {
        $isUserAllowed = $this->getUser()->isAllowed(self::ExtensionName, Privilege::Add);

        if (!$isUserAllowed) {
            $this->flashMessage(self::ErrorPermissionAdd);
            $this->redirect(self::RedirectLink);
        }
    }

    public function createComponentAddForm(): Form
    {
        $form = new Form();

        $form->setTranslator($this->translator);

        $form->addText("title", "textPageModule.edit.form.title.label")
            ->setRequired();

        $form->addTextarea("content", "textPageModule.edit.form.content.label");

        $form->addText("slug", "textPageModule.edit.form.slug.label")
            ->addRule($form::Pattern, "textPageModule.edit.form.slug.validations.pattern", '^[a-z0-9]+(?:-[a-z0-9]+)*$')
            ->addRule($form::IS_NOT_IN, "textPageModule.edit.form.slug.validations.isNotIn", $this->textPageService->getAllSlugs())
            ->setRequired();

        $form->addSubmit("submit", "textPageModule.edit.form.submit.label");

        $form->onSuccess[] = [$this, "addFormSubmitSuccess"];

        return $form;
    }

    public function addFormSubmitSuccess(Form $form, ArrayHash $values)
    {
        try {
            $this->textPageService->addItem($values);
            $this->flashMessage(new FlashMessage(self::AddSuccess, FlashMessageType::Success));
        } catch (Exception $e) {
            Debugger::log($e->getMessage(), ILogger::EXCEPTION);
            $this->flashMessage(new FlashMessage(self::AddFailed, FlashMessageType::Danger));
        }

        $this->redirect(self::RedirectLink);
    }
}
