<?php

declare(strict_types=1);

namespace JaAdmin\TextPageModule\Presenters;

use Exception;
use JaAdmin\CoreModule\Utils\FlashMessage;
use JaAdmin\CoreModule\Utils\FlashMessageType;
use JaAdmin\CoreModule\Utils\Privilege;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;
use Tracy\ILogger;

final class EditPresenter extends BasePresenter
{
    private const RedirectLink = ":TextPage:Overview:default";
    private const PermissionFail = "textPageModule.edit.flashMessage.permissionFail";
    private const EditSuccess = "textPageModule.edit.flashMessage.editSuccess";
    private const EditFail = "textPageModule.edit.flashMessage.editFail";

    public string $id;

    public function actionDefault(string $id)
    {
        $isUserAllowed = $this->getUser()->isAllowed(self::ExtensionName, Privilege::Edit);

        if (!$isUserAllowed) {
            $this->flashMessage(self::ErrorPermissionEdit);
            $this->redirect(self::RedirectLink);
        }

        $this->id = $id;
    }

    public function createComponentEditForm(): Form
    {
        $form = new Form();

        $form->setTranslator($this->translator);

        $item = $this->textPageService->getItem($this->id);

        $form->addHidden("id");

        $form->addText("title", "textPageModule.edit.form.title.label")
            ->setRequired();

        $form->addTextArea("content", "textPageModule.edit.form.content.label");

        $form->addText("slug", "textPageModule.edit.form.slug.label")
            ->addRule($form::Pattern, "textPageModule.edit.form.slug.validations.pattern", '^[a-z0-9]+(?:-[a-z0-9]+)*$')
            ->addRule($form::IS_NOT_IN, "textPageModule.edit.form.slug.validations.isNotIn", $this->textPageService->getAllSlugs($item->getSlug()))
            ->setRequired();

        $form->addSubmit("submit", "textPageModule.edit.form.submit.label");

        $form->setDefaults([
            "id" => $item->getId(),
            "title" => $item->getTitle(),
            "content" => $item->getContent(),
            "slug" => $item->getSlug()
        ]);

        $form->onSuccess[] = [$this, "editFormSubmitSuccess"];

        return $form;
    }

    public function editFormSubmitSuccess(Form $form, ArrayHash $values)
    {
        try {
            $this->textPageService->editItem($values);
            $this->flashMessage(new FlashMessage(self::EditSuccess, FlashMessageType::Success));
        } catch (Exception $e) {
            Debugger::log($e->getMessage(), ILogger::EXCEPTION);
            $this->flashMessage(new FlashMessage(self::EditFailed, FlashMessageType::Danger));
        }

        $this->redirect(self::RedirectLink);
    }
}
