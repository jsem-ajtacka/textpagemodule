<?php

declare(strict_types=1);

namespace JaAdmin\TextPageModule\Services;

use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use JaAdmin\CoreModule\Models\EntityManagerDecorator;
use JaAdmin\CoreModule\Utils\KW;
use JaAdmin\TextPageModule\Models\TextPage;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;

class TextPageService
{
    private ObjectRepository $repository;

    public function __construct(private EntityManagerDecorator $em)
    {
        $this->repository = $this->em->getRepository(TextPage::class);
    }

    public function getItem(string $id) : TextPage|null
    {
        return $this->repository->findOneBy([KW::Id => $id]);
    }

    public function getItems()
    {
        return $this->repository->createQueryBuilder(KW::Entity, KW::Entity . KW::Dot . KW::Id)->getQuery()->getResult();
    }

    /**
     * @throws Exception
     */
    public function addItem(ArrayHash $values) : TextPage
    {
        $textPage = new TextPage();
        return $this->saveItem($textPage, $values);
    }

    /**
     * @throws Exception
     */
    public function editItem(ArrayHash $values) : TextPage
    {
        $textPage = $this->getItem($values->id);
        return $this->saveItem($textPage, $values);
    }

    public function delete(string $id): void
    {
        $item = $this->getItem($id);
        $this->em->remove($item);
        $this->em->flush();
    }

    private function saveItem(TextPage $textPage, ArrayHash $values): TextPage
    {
        $textPage->setTitle($values->title)
            ->setContent($values->content)
            ->setSlug($values->slug);

        $this->em->persist($textPage);
        $this->em->flush();

        return $textPage;
    }

    public function getQueryBuilder(): QueryBuilder
    {
        return $this->repository->createQueryBuilder(KW::Entity, KW::Entity . KW::Dot . KW::Id);
    }

    public function getAllSlugs(string|null $except = null): array
    {
        $qb = $this->repository->createQueryBuilder(KW::Entity)
            ->select(KW::Entity . KW::Dot . KW::Slug);

        if (!is_null($except)) {
            $qb->where(KW::Entity . KW::Dot . KW::Slug . " != '" . $except . "'");
        }

        return $qb->getQuery()->getSingleColumnResult();
    }
}
