<?php

declare(strict_types=1);

namespace JaAdmin\TextPageModule\DI;

use Nette\DI\CompilerExtension;

final class TextPageModuleExtension extends CompilerExtension {}
