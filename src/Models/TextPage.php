<?php

declare(strict_types=1);

namespace JaAdmin\TextPageModule\Models;

use JaAdmin\CoreModule\Models\BaseEntity;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EntityRepository::class)]
#[ORM\Table(name: "text_page")]
class TextPage extends BaseEntity
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]
    protected string $id;

    #[ORM\Column(type: "string")]
    protected string $title;

    #[ORM\Column(type: "text")]
    protected string $content;

    #[ORM\Column(type: "string", unique: true)]
    protected string $slug;

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }
}
